def loopTest() {
    def length = 10
    def a = b = c = 1

    for (int i=0; i < length; i++) {
        a += 1
    }

    for (i in 0..length-1) {
        b += 1
    }

    length.times {
        c +=1
    }

    assert a == b
    assert b == c
    assert a == c
}

def arrayTest() {
    def array = [7, 25, 9]
    def (a, b, c) = array

    assert array.size() == 3
    assert array[1] == 25
    assert array[0] == a
    assert array[2] == c
}

def mapTest() {
    def map = ['one':'1', 'two':2, "three":'3', 'four':4]

    def size = 0
    map.each{
        size += 1
    }

    assert map.size() == size
    assert map['one'] == '1'
    assert map['one'] != 1
}

def testSuit() {
    loopTest()
    arrayTest()
    mapTest()
}

testSuit()