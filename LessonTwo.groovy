import groovy.io.FileType

path = "D:\\GeekHub_2014\\MyFolder\\"
def dir = new File(path)

dir.eachFileRecurse(FileType.FILES) { file ->
    if (file.name ==~ /a_.*?/) {
        file.append(System.getProperty("line.separator") + "Number of 'a' - " + file.getText().findAll{it == "a"}.size())
        file.renameTo(new File(path + "done_" + file.name))
    } else if (file.name ==~ /1_.*?/) {
        def matrix= new Object[4][4]
        Random rand = new Random()

        file.delete()
        def sumMainDiagonal = 0
        def sumAntiDiagonal = 0

        (0..<matrix.size()).each{ i ->
            (0..<matrix[i].size()).each{ j ->
                matrix[i][j] = rand.nextInt(100)
                file << matrix[i][j] + " "
                if (i == j) {
                    sumMainDiagonal += matrix[i][j]
                }
                if ((i + j) == matrix.length - 1) {
                    sumAntiDiagonal += matrix[i][j]
                }
            }
            file << System.getProperty("line.separator")
        }

        file << System.getProperty("line.separator")
        file << "Sum of main diagonal - " + sumMainDiagonal
        file << System.getProperty("line.separator")
        file << "Sum of anti diagonal - " + sumAntiDiagonal
        file << System.getProperty("line.separator")
        file << "Rezult - " + (sumMainDiagonal + sumAntiDiagonal)

        file.renameTo(new File(path + "done_" + file.name))
    } else if (file.name ==~ /d_.*?/) {
        file.write(new Date().format("​yyyy-MM-dd HH-mm-ss"))
        file.renameTo(new File(path + "done_" + file.name))
    }
}



